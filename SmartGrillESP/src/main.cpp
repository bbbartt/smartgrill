#include <ESP8266WiFi.h>

#include <DNSServer.h>            //Local DNS Server used for redirecting all requests to the configuration portal
#include <ESP8266WebServer.h>     //Local WebServer used to serve the configuration portal
#include <WiFiManager.h>          //https://github.com/tzapu/WiFiManager WiFi Configuration Magic

#include <WiFiClient.h>
#include <ESP8266WiFiMulti.h> 
#include <ESP8266mDNS.h>


ESP8266WebServer server(3232);    // Create a webserver object that listens for HTTP request on port 80

void httpSetup();
void handleRoot();              // function prototypes for HTTP handlers
void handleWifiSetup();
void handleNotFound();
void handleTurnOn();
void handleTurnOff();


#define relayPin D1

void setup(){
  //WiFi.disconnect();      //erase stored credentials... //remove this!

  Serial.begin(115200);

  pinMode(relayPin, OUTPUT);

  WiFiManager wifiManager;

  wifiManager.autoConnect("SmartGrill Setup");

 // Serial.print(wifiManager.ip);

  Serial.println("Connected to AP, entering http setup...");
  httpSetup();
}



void loop(){
  server.handleClient();    //listen for HTTP requests...

  // digitalWrite(relayPin, 1);
  // delay(100);
  // digitalWrite(relayPin, 0);
  // delay(100);
}




void httpSetup(){

  server.on("/", HTTP_GET, handleRoot);     // Call the 'handleRoot' function when a client requests URI "/"
  server.on("/wifiSetup", HTTP_POST, handleWifiSetup);  // Call the 'handleLED' function when a POST request is made to URI "/LED"
  server.on("/turnOn", HTTP_POST, handleTurnOn);
  server.on("/turnOff", HTTP_POST, handleTurnOff);
  server.onNotFound(handleNotFound);        // When a client requests an unknown URI (i.e. something other than "/"), call function "handleNotFound"

  server.begin();                           // Actually start the server
  Serial.println("HTTP server started");


}



void handleRoot() {                         // When URI / is requested, send a web page with a button to toggle the LED
  server.send(200, "text/html", "<form action=\"/LED\" method=\"POST\"><input type=\"submit\" value=\"Toggle LED\"></form>");
}

void handleWifiSetup() {                          // If a POST request is made to URI /LED
  char body;
  //server.sendHeader("Location","/");        // Add a header to respond with a new location for the browser to go to the home page again
  server.send(200, "application/json", "body");                         // Send it back to the browser with an HTTP status 303 (See Other) to redirect
}


void handleTurnOn(){
  Serial.print("turned on");

  //turnon
  digitalWrite(relayPin, 1);
    server.send(200, "application/json", "body");                         // Send it back to the browser with an HTTP status 303 (See Other) to redirect

}


void handleTurnOff(){
  Serial.print("turned off!");

  //turnon
  digitalWrite(relayPin, 0);
    server.send(200, "application/json", "body");                         // Send it back to the browser with an HTTP status 303 (See Other) to redirect

}





void handleNotFound(){
  Serial.println("retreived HTTP, but idk what to do with it...");
  server.send(404, "text/plain", "404: Not found"); // Send HTTP status 404 (Not Found) when there's no handler for the URI in the request
}