import { Component, Injectable } from '@angular/core';
import { IonicPage, NavController, NavParams } from 'ionic-angular';

import { Storage } from '@ionic/storage';

import { AlertController } from 'ionic-angular';
import { HTTP } from '@ionic-native/http';




/**
 * Generated class for the TogglePage page.
 *
 * See https://ionicframework.com/docs/components/#navigation for more info on
 * Ionic pages and navigation.
 */

 //init...
 let fromDismiss =  false;


@IonicPage()
@Component({
  selector: 'page-toggle',
  templateUrl: 'toggle.html',
})

@Injectable()
export class TogglePage { 
  public isToggled: boolean;
  

  constructor(public navCtrl: NavController, public navParams: NavParams, private http:HTTP, private storage: Storage, private alertCtrl: AlertController) {
    // this.isToggled = false;
    
  }

  ionViewDidLoad() {
    console.log('ionViewDidLoad TogglePage');
  }

  //on buttonclick
  onClick(){
    window.open('https://www.tostiszijnnetmensen.nl', '_system'); 
    /*
    window.open(‘http://example.com’, ‘_system’);	Loads in the system browser
    window.open(‘http://example.com’, ‘_blank’);	Loads in the InAppBrowser
    window.open(‘http://example.com’, ‘_blank’, ‘location=no’);	Loads in the InAppBrowser with no location bar
    window.open(‘http://example.com’, ‘_self’);	Loads in the Cordova web view
    */

    return false;
  }



  public notify() {
    //only do it when the user prompted the interaction. Not when the slider is returned due to errors... 
    if(fromDismiss == false){

    
      if(this.isToggled == true){
        console.log("Turn on!");

        //obtain ip & port
        let ipPort;
        this.storage.get('ipPort').then((val) => {
          //console.log('ipPort:', val);
          ipPort = val + "/turnOn"
          console.log(ipPort);

          if(ipPort == null){
            // let alert = this.alertCtrl.create({
            //   title: 'No configured adress',
            //   subTitle: 'Please check ip and port configuration at settings.',
            //   buttons: ['Dismiss']
            // });
            // alert.present();
            this.isToggled = true;
            }
          else{

            this.http.post(ipPort, {}, {})
            .then(data => {
              console.log(data.status);
              console.log(data.data);
              console.log(data.headers);
              if(data.status == 200){
                //set ip adress and port to storage
      
              }
              else{
                // this.isToggled = false;
              }
            })
            .catch(error => {
              console.log(error.status);
              console.log(error.error);
              console.log(error.headers);

              let alert = this.alertCtrl.create({
                title: 'IP/Port error',
                subTitle: 'Please check ip and port configuration at settings.',
                buttons: [{
                  text: 'Dismiss',
                  handler: () =>{
                    this.isToggled = !this.isToggled;
                    fromDismiss = true;
                  }
                }]
              });
              alert.present();
              
            });

          }


        });


      }



      else if(this.isToggled == false){
        console.log("Turn off!");


        //obtain ip & port
        let ipPort;
        this.storage.get('ipPort').then((val) => {
          //console.log('ipPort:', val);
          ipPort = val + "/turnOff";
          console.log(ipPort);

          if(ipPort == null){

            }
          else{
            //http PUT 0
            this.http.post(ipPort, {}, {})
            .then(data => {
              console.log(data.status);
              console.log(data.data);
              console.log(data.headers);
              if(data.status == 200){
                //set ip adress and port to storage
      
              }
              else{
                //error
              }
            })
            .catch(error => {
              console.log(error.status);
              console.log(error.error);
              console.log(error.headers);

              let alert = this.alertCtrl.create({
                title: 'No configured adress',
                subTitle: 'Please check ip and port configuration at settings.',
                buttons: [{
                  text: 'Dismiss',
                  handler: () =>{
                    this.isToggled = !this.isToggled;
                    fromDismiss = true;
                    
                  }
                }]
              });
              alert.present();
              
              
            });

          }


        });
      }

    }
    
  
    
  
    else{
    fromDismiss = false;
    }
  
  }


}
