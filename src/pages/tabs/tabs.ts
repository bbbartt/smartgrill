import { Component } from '@angular/core';

import { TogglePage } from '../toggle/toggle';
import { SettingsPage } from '../settings/settings';
import { AlarmPage } from '../alarm/alarm';
import { HomePage } from '../home/home';

@Component({
  templateUrl: 'tabs.html'
})
export class TabsPage {

  tab1Root = HomePage;
  tab2Root = TogglePage;
  tab3Root = AlarmPage;
  tab4Root = SettingsPage;

  constructor() {

  }
}
