import { Component } from '@angular/core';
import { IonicPage, NavController, NavParams } from 'ionic-angular';
import { Text } from '@angular/compiler';
import {FormGroup, FormBuilder, FormControl, Validators} from "@angular/forms";
import { HTTP } from '@ionic-native/http';

import { Storage } from '@ionic/storage';
import { Placeholder } from '@angular/compiler/src/i18n/i18n_ast';

/**
 * Generated class for the SettingsPage page.
 *
 * See https://ionicframework.com/docs/components/#navigation for more info on
 * Ionic pages and navigation.
 */

declare var angular: any;



@IonicPage()
@Component({
  selector: 'page-settings',
  templateUrl: 'settings.html',

  ////
  
  ////
})
export class SettingsPage {
  ip:number;
  port:number;
  connectedOrNot: string = "Not Connected"; // Default is 0

  
  
  constructor(public navCtrl: NavController, public navParams: NavParams, private http:HTTP, private storage: Storage ) {

  }


  ionViewDidLoad() {
    console.log('ionViewDidLoad SettingsPage');

  }

  ionViewWillEnter(){
      let stringReq; 
      
      this.storage.get('ipPort').then((val) => {
        //console.log('ipPort:', val);
        stringReq = val + "/wifiSetup"
      });

      console.log(stringReq);
      // let stringReq = "http://" + String(this.ip) + ":" + String(this.port)  + "/wifiSetup";
      
      this.http.post(stringReq, {}, {})
      .then(data => {
        console.log(data.status);
        console.log(data.data);
        console.log(data.headers);
        if(data.status == 200){
          this.connectedOrNot = "Connected";
          //set ip adress and port to storage

        }
        else{
          this.connectedOrNot = "Error. Check IP and Port."
        }
      })
      .catch(error => {
        console.log(error.status);
        console.log(error.error);
        console.log(error.headers);
        this.connectedOrNot = "Error. Check IP and Port."
        
      });

  }


  onClick(){
    // console.log(this.ip);
    // console.log(":");
    // console.log(this.port);

    let stringReq = "http://" + String(this.ip) + ":" + String(this.port)  + "/wifiSetup";
    let httpString = "http://" + String(this.ip) + ":" + String(this.port);
    console.log(stringReq);
    this.storage.set('ipPort', httpString);  
    this.storage.set('ip', httpString);     
    this.storage.set('port', httpString);     
    
    

    
    this.http.post(stringReq, {}, {})
      .then(data => {
        console.log(data.status);
        console.log(data.data);
        console.log(data.headers);
        if(data.status == 200){
          this.connectedOrNot = "Connected";
          //set ip adress and port to storage
  
        }
        else{
          this.connectedOrNot = "Error. Check IP and Port."
        }
      })
      .catch(error => {
        console.log(error.status);
        console.log(error.error);
        console.log(error.headers);
        this.connectedOrNot = "Error. Check IP and Port."
        
      });




  }

}

